bool wifi_status() {
  if (WiFi.status() == WL_CONNECTED) return true;
  else return false;
}

void wifi_update() {
  if (debug) Serial.println("Wifi_setup");
  if (!wifi_status()) {
    byte i = 0;
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    //пробуем подконектится 10 раз с интервалом в 3 сек.
    while (i < 4 && !wifi_status()) {
      Serial.print("_");
      Serial.print(i);
      delay(3000);
      i++;
    }
    Serial.println();
    if (wifi_status()) {
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    } else Serial.println("Sync stopped, wifi is not found");
  }
}
