void motor_init() {
  //if (debug) Serial.println("motor init");
  stepper.setMaxSpeed(500);
  stepper.setSpeed(500);
  stepper.setAcceleration(500);
}

void Main() {
  getTime();
  if (weekday() != 0) {
    //в восркесенье не кормим
    // if (debug) Serial.println("week check");
    if (feed_cur_count <= feed_max_count) {
      //значит заряды в кормушке еще есть
      if (curTime[0] == feed_on )
        if (curTime[1] >= 2 && curTime[1] < 3) {
          //  if (debug) Serial.println("feed");
          feed();//кормим
          feed_cur_count++;
          //пишем в базу
          writeEEPROM(feed_cur_count, 0);
          delay(120000);
        }
      //if (debug) Serial.println("fcpd>1");
      //предполагается что кормлений может быть 2 в сутки
      if (feed_count_per_day == 2)
        //проверяем что заряды еще остались
        if (feed_cur_count <= feed_max_count) {
          if (curTime[0] == feed_on + feed_time_interval)
            if (curTime[1] >= 2 && curTime[1] < 3) {
              //    if (debug) Serial.println("feed_2");
              feed();//кормим
              feed_cur_count++;
              //пишем в базу
              writeEEPROM(feed_cur_count, 0);
              delay(120000);
            }
        }
    }
  }
}

void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(' ');
  Serial.print(day());
  Serial.print(' ');
  Serial.print(month());
  Serial.print(' ');
  Serial.print(year());
  Serial.println();
}

void printDigits(int digits)
{
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(':');
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

//метод для получения текущего времени
void getTime() {
  curTime[0] = hour();
  curTime[1] = minute();
  if (debug)digitalClockDisplay();
}


//Метод управления мотором. кол-во оборотов и направление
void feed() {
  if (debug) Serial.println("motor on");
  stepper.setCurrentPosition(0);
  stepper.runToNewPosition(steps - feed_cur_count / 2);
  stepper.disableOutputs();
}

bool chekTime() {
  //если время не установлено то синхронизиуем каждые 5 мин
  if (timeStatus() == timeNotSet) {
    setSyncInterval(300);
    if (debug) Serial.println("timeNotSet");
    return 0;
  } else if (timeStatus() == timeNeedsSync ) {
    setSyncInterval(3600);
    if (debug) Serial.println("timeNeedsSync");
    return 1;
  } else {
    setSyncInterval(259200);
    if (debug) Serial.println("timeSync");
    return 1;
  }
}

void set_time() {
  //2020,11,12,15,16,00
  // check for input to set the RTC, minimum length is 12, i.e. yy,m,d,h,m,s
  if (Serial.available() >= 12) {
    static time_t tLast;
    time_t t;
    tmElements_t tm;
    // note that the tmElements_t Year member is an offset from 1970,
    // but the RTC wants the last two digits of the calendar year.
    // use the convenience macros from the Time Library to do the conversions.
    int y = Serial.parseInt();
    if (y >= 100 && y < 1000) {}
    // Serial << F("Error: Year must be two digits or four digits!") << endl;
    else {
      if (y >= 1000)
        tm.Year = CalendarYrToTm(y);
      else    // (y < 100)
        tm.Year = y2kYearToTm(y);
      tm.Month = Serial.parseInt();
      tm.Day = Serial.parseInt();
      tm.Hour = Serial.parseInt();
      tm.Minute = Serial.parseInt();
      tm.Second = Serial.parseInt();
      t = makeTime(tm);
      // RTC.set(t);        // use the time_t value to ensure correct weekday is set
      setTime(t);
      // Serial << F("RTC set to: ");
      digitalClockDisplay();
      //  Serial << endl;
      // dump any extraneous input
      while (Serial.available() > 0) Serial.read();
    }
  }
}
