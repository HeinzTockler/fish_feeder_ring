///адреса хранения данных о поливе
#define feed_cur_count_adr 0
#define feed_on_adr (feed_cur_count_adr+sizeof(int))
#define feed_count_per_day_adr (feed_on_adr+sizeof(int))

void checkEEPROM() {
  int fl1 = 255;
  int fl2 = 255;
  int fl3 = 255;

  EEPROM.begin(50);
  EEPROM.get(feed_cur_count_adr, fl1);
  EEPROM.get(feed_on_adr, fl2);
  EEPROM.get(feed_count_per_day_adr, fl3);

  if (debug) Serial.print("feed_cur_count_adr= ");
  if (debug) Serial.print(fl1);
  if (debug) Serial.println(" ");

  if (debug) Serial.print("feed_on_adr= ");
  if (debug) Serial.print(fl2);
  if (debug) Serial.println(" ");

  if (debug) Serial.print("feed_count_per_day_adr= ");
  if (debug) Serial.print(fl3);
  if (debug) Serial.println(" ");
  //  if (debug)Serial.println(message);

  //кусок тупого кода, не хотелось с массивом заморачиваться
  if (fl1 != feed_cur_count_adr & fl1 != 0 & fl1 != 255 & fl1 > 0 & fl1 < 100)feed_cur_count = fl1;
  if (fl2 != feed_on_adr & fl2 != 0 & fl2 != 255 & fl2 > 0 & fl2 < 100)feed_on = fl2;
  if (fl3 != feed_count_per_day_adr & fl3 != 0 & fl3 != 255 & fl3 > 0 & fl3 < 100)feed_count_per_day = fl3;

  EEPROM.end();
}

bool writeEEPROM(int val, byte adr) {
  int param = 0;
  if (debug) Serial.print("adr= ");
  if (debug) Serial.print(adr);
  if (debug) Serial.println(" ");
  EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.print("param= ");
  if (debug) Serial.print(param);
  if (debug) Serial.println(" ");
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    EEPROM.commit();
    return true;
  } else return false;
  EEPROM.end();
}

bool writeEEPROM_str(String val, byte adr) {
  String param ;
  EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.println(param);
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    EEPROM.commit();
    return true;
  } else return false;
  EEPROM.end();
}
